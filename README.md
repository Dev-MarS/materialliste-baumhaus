# Materialliste-Baumhaus 🌳🏠

This is a beginner repository to exercise the use of Git. Exercise the use of "cloning" and "pushing" with Git changes.

## Setup of user verification

There are two secure ways of cloning and pushing your code into a _remote_ repository like GitLab: over SSH and https. 

In order to push your work to a remote repository platform like GitLab, you need to setup an account. [Sign up](https://gitlab.com/) for an account. 

### SSH
First, create a SSH key on your computer in your terminal or Git bash with the following command:

```
ssh-keygen -t ed25519 -C "your_email@example.com"
```

The SSH key will be in the following file path: ~/.ssh/...

In GitLab signed in, you can setup your [SSH](https://en.wikipedia.org/wiki/Secure_Shell) connection: 
- click on your profile picture --> "Preferences" 
- Click on "SSH keys"
- Click on "Add new key"
- Copy your "**Public** SSH Key"*, set the usage type and expiration date and save.
You are done. :-) 

*Never, ever, share your private SSH key! One way to see which key is public and which key is private is the ending of the file name on your computer: The **public key** file on your computer ends with id_ed25519**.pub**__. The **private key** has no file type end like: id_ed25519.  

### https

For this method you just need your _username_ and _password_ of your GitLab account.

## Using Git with SSH and https

Download the remote repository by copying the **clone** address (the blue button on the top right side) on this repository page. Click on the button and choose either the code address for a *ssh* or *https* connection.

On your local git client (=your computer), for example in your [terminal on a Linux system](https://wiki.ubuntuusers.de/Terminal/) or [git bash on a windows system](https://git-scm.com/downloads) use the code address with the following git command:

SSH: 
 ```
 git clone git@gitlab.com:Dev-MarS/materialliste-baumhaus.git
 
```

https: 
 ```
 git clone https://gitlab.com/Dev-MarS/materialliste-baumhaus.git
 
```

cloning = copying the code from remote (GitLab.com) to your local working environment.
pushing = pushing (~uploading) your code from your local working environment to a remote repository (GitLab.com) 


## Next steps 

Now, with the code locally on your computer, do some changes to the list of materials in the file "Materialien.txt". Afterwards, save your changes in git (=commit your changes like: git add Materialien.txt --> git commit -m "Add more materials") and push your changes to this repository with SSH or https. 

If done, then congratulations: You have successfully cloned and pushed your changes! ✨🎉🙌✨ 

